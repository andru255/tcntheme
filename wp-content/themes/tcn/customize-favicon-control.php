<?php
if(class_exists('WP_Customize_Image_Control')){
    class Custom_Favicon_Control extends WP_Customize_Image_Control {
        public function __construct($manager, $id, $args){
            $this->extensions[] = 'ico';
            return parent::__construct($manager, $id, $args);
        }
    }
}
?>