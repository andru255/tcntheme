<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
//define('DB_NAME', 'tcntheme');
define('DB_NAME', 'tcn');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '1234');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'G61:vi+0*Ck9-B@ E?6=k+,_TzvHpue+*@F-<>Pv9O%^`%mts_DN0}bLzq8Zzt6-');
define('SECURE_AUTH_KEY',  'g$?]HNw`v5WM8_-*_.:.s`4De4|l+=c/uF|v-|kkSk&RL-=S=NWBt,sTp};%aeSE');
define('LOGGED_IN_KEY',    'fI]@Aj3$u6dUrsES<;CPewQ!t[.h/XF(i:MAP[oxz2U0t+3+^([ggE,*(0K{ed%$');
define('NONCE_KEY',        '{/V:qyLrqput?eT4?;S=L%F3U||A:ZMnb/vc0y}.;7*obms=yTa;V>C{nF6M3/z)');
define('AUTH_SALT',        ',]HH7,nZGqa^/j,|x^2!W}Z>4a;+;B_i>=8Pha,5+7(#ev!:M^EE^Ck/GC2nXX:!');
define('SECURE_AUTH_SALT', '_Enf**b?haI<mx8-x|c]E,GRJ$PRmqp_je*O?yrqhGFE|Xghf<vuUXTzzYu=xSal');
define('LOGGED_IN_SALT',   '79y5t?U2Z+z^dW|ywltVgb:L~%)?[2r*1JR$?8;u@Trp?TwI-lCj0r^fXq% PIO6');
define('NONCE_SALT',       '=a*`Qv!cTROI,|wLt89U@y>IZo*/Wdft )o8C$f0qfr<aitR+~|?FOCQC~)#YS`q');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
ini_set('error_reporting', E_ALL);
/* That's all, stop editing! Happy blogging. */

/*para instalar plugins en local*/
define('FS_METHOD', 'direct');

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
