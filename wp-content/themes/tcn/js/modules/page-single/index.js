yOSON.AppCore.addModule("modal-postular", function(){
    var st = {
        btnLaunchModal: ".b-postular__btn",
        frmModal: "#frmPostular"
    };
    var DOM = {};
    var catchDOM = function(){
        DOM.btnLaunchModal = $(st.btnLaunchModal);
        DOM.frmModal = $(st.frmModal);
    };
    var subscribeEvents = function(){
        DOM.btnLaunchModal.on('click', events.showFormPostular);
    };
    var events = {
        showFormPostular: function(evt){
            $.fancybox.open(DOM.frmModal.html());
            evt.preventDefault();
        }
    };
    return {
        init: function(){
            $(function(){
                catchDOM();
                subscribeEvents();
            });
        }
    };
}, ["js/libs/fancybox/source/jquery.fancybox.pack.js"]);
yOSON.AppCore.runModule("modal-postular");
