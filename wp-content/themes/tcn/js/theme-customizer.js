(function($){
   wp.customize( 'tcntheme_advanced_options_header_color', function(value){
       value.bind( function(newValue){
           $(".b-main-menu").css("background-color", newValue);
       } );
   } );
   wp.customize( 'tcntheme_advanced_options_menu_background_color', function(value){
       value.bind( function(newValue){
           $(".b-menu__link").css("background-color", newValue);
       } );
   } );
   wp.customize( 'tcntheme_advanced_options_menu_font_color', function(value){
       value.bind( function(newValue){
           $(".b-menu__link").css("color", newValue);
       } );
   } );
   wp.customize( 'tcntheme_advanced_options_footer_color', function(value){
       value.bind( function(newValue){
           $(".b-footer, .b-footer__inner-footer").css("background-color", newValue);
       } );
   } );

})(jQuery);
