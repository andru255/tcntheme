<?php
 class Custom_Walker_Nav_Menu extends Walker_Nav_Menu {
     function start_el(&$output, $item, $depth =0, $args = array(), $id = 0){
         //global $wp_query;
         global $post;
         //get the indent
         $indent = ($depth)? str_repeat("\t", $depth): "";
         //set the variables $class_names and $value
         $class_names = $value = "";
         //get the classes
         $classes = empty($item->classes)? array(): (array) $item->classes;
         //set the value of $class_names
         $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item));
         $class_names = 'class="'. esc_attr($class_names).'"';

         //append the class when is current
         $classLi = "";

         //select current
         //$currentUrl = "http://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI];
         if(in_array('current-menu-item', $classes)){
             $classLi = "active";
         }
         //setting the output
         //original
         $output.= "<li class='b-menu__item'>";

         //dealing with the attributes
         $attributes  = 'class="'. $classLi .' b-menu__link"';
         $item_output = !empty($args->before)? $args->before: '';
         if($item->url){
            $attributes .= !empty($item->title)? ' title="'. esc_attr($item->title). '"':'';
            $attributes .= !empty($item->target)? ' target="'. esc_attr($item->target). '"':'';
            $attributes .= !empty($item->xfn)? ' rel="'. esc_attr($item->xfn). '"':'';
            $attributes .= !empty($item->url)? ' href="'. esc_attr($item->url). '"':'';

            $item_output .= "<a ". $attributes .">";
            $item_output .= $item->title;
            $item_output .= "</a>";
            $item_output .= $args->after;
         } else {
            $attributes .= !empty($item->title)? ' title="'. esc_attr($item->post_title). '"':'';
            $attributes .= !empty($item->target)? ' target="'. esc_attr($item->target). '"':'';
            $attributes .= !empty($item->xfn)? ' rel="'. esc_attr($item->xfn). '"':'';
            $attributes .= ' href="'. get_permalink($item->ID). '"';

            $item_output .= "<a ". $attributes .">";
            $item_output .= $item->post_title;
            $item_output .= "</a>";
            $item_output .= !empty($args->after)? $args->after: '';
         }

         $output.= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
         $output.="</li>";
     }

     function end_el(&$output, $item, $depth =0, $args = array()){

     }

 }
