
<?php /**
* Template Name: Page Single
*/?><!DOCTYPE html>
<html <?php language_attributes();?>>
  <head>
    <meta charset="<?php bloginfo('charset')?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="language" content="es">
    <title><?php wp_title('|', true, 'right')?></title><?php wp_head(); ?>
    <link href="<?php echo get_template_directory_uri();?>/css/all-fonts.css?aleatoryxD" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo get_template_directory_uri();?>/css/all-all.css?aleatoryxD" media="all" rel="stylesheet" type="text/css"><?php if( get_theme_mod('tcntheme_advanced_options_favicon') ):?>
    <link href="<?php echo get_theme_mod('tcntheme_advanced_options_favicon');?>?aleatoryxD" rel="icon" type="image/vnd.microsoft.icon">
    <link href="<?php echo get_theme_mod('tcntheme_advanced_options_favicon');?>?aleatoryxD" rel="shortcut icon" type="image/vnd.microsoft.icon">
    <link href="<?php echo get_theme_mod('tcntheme_advanced_options_favicon');?>?aleatoryxD" rel="shortcut icon" type="image/x-icon">
    <link href="<?php echo get_theme_mod('tcntheme_advanced_options_favicon');?>?aleatoryxD" rel="shortcut icon" type="image/jpeg"><?php else:?>
    <link href="<?php echo get_template_directory_uri();?>/static/favicon.ico?aleatoryxD?aleatoryxD" rel="icon" type="image/vnd.microsoft.icon">
    <link href="<?php echo get_template_directory_uri();?>/static/favicon.ico?aleatoryxD?aleatoryxD" rel="shortcut icon" type="image/vnd.microsoft.icon">
    <link href="<?php echo get_template_directory_uri();?>/static/favicon.ico?aleatoryxD?aleatoryxD" rel="shortcut icon" type="image/x-icon">
    <link href="<?php echo get_template_directory_uri();?>/static/favicon.ico?aleatoryxD?aleatoryxD" rel="shortcut icon" type="image/jpeg"><?php endif;?>
    <link href="<?php echo get_template_directory_uri();?>/css/page-single.css?aleatoryxD" media="all" rel="stylesheet" type="text/css">
    <script src="<?php echo get_template_directory_uri();?>/js/libs/jquery/dist/jquery.min.js?aleatoryxD" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/libs/yosonjs/build/yoson-min.js?aleatoryxD" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/modules/all/all.js" type="text/javascript"></script>
  </head>
  <body <?php body_class();?>>
    <!--start header-->
    <header class="b-header">
      <div class="b-powered">
        <div class="b-powered__content"><a href="#" title="Aptitus" class="b-powered__link">&nbsp; </a>
        </div>
      </div>
      <div class="b-main-menu">
        <div class="b-main-menu__content">
          <div class="b-company-logo"><?php if( get_theme_mod('tcntheme_advanced_options_logo') ):?><a href="<?php echo esc_url(home_url('/'));?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="b-company-logo__logo"><img src="<?php echo get_theme_mod('tcntheme_advanced_options_logo');?>" class="b-company-logo__img" alt=""/></a><?php else:?><a href="<?php echo esc_url(home_url('/'));?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="b-company-logo__logo"><img src="<?php echo get_template_directory_uri();?>/static/img/default-logo.png" class="b-company-logo__img" alt=""/></a><?php endif;?>
          </div>
          <!-- no logueado-->
          <div class="b-nav-login"><a href="javascript:;" class="b-nav-login__login">Ingresa</a> |  <a href="javascript:;" class="b-nav-login__signup">Registrate</a>
          </div>
          <!-- logueado-->
          <div class="b-user-menu"><span class="icon-menu-arrowdown"></span><a href="javascript:;" class="b-user-menu__nickname">Hola <span class="b-user-menu__name">XXXX</span></a>
            <div class="b-user-menu__down b-user-menu__down_menu">
              <ul>
                <li><a href="#">Alertas</a></li>
                <li><a href="#">Configuracion</a></li>
                <li><a href="#">Asociar a redes</a></li>
                <li><a href="#">Privacidad</a></li>
                <li><a href="#">Destacate</a></li>
                <li><a href="#" class="logout">
                     
                    Cerrar sesion<span class="icon-cerrar-sesion"></span></a></li>
              </ul>
            </div>
          </div>
          <div class="b-responsive-user-menu"><a href="javascript:;" class="icon-sandwich"></a>
            <div class="b-responsive-user-menu__down b-responsive-user-menu__down_menu">
              <div class="b-responsive-user-menu__overlay">
              </div>
              <?php wp_nav_menu(
                         array(
                            'theme_location'=> 'primary',
                            'menu_class'=> 'b-menu',
                            'container'=> 'ul',
                            'menu_id'=> 'primary-menu',
                            'walker'=> new Custom_Walker_Nav_Menu,
                            )
                      );?>
            </div>
          </div>
        </div>
      </div>
      <div class="b-main-banner"><?php if( get_theme_mod('tcntheme_advanced_options_banner_image') ):?><img src="<?php echo get_theme_mod('tcntheme_advanced_options_banner_image');?>?aleatoryxD"><?php else:?><img src="<?php echo get_template_directory_uri();?>/static/img/banner-home.jpg"><?php endif;?>
        <div class="b-main-banner__content">
          <?php wp_nav_menu(
                     array(
                        'theme_location'=> 'primary',
                        'menu_class'=> 'b-menu',
                        'container'=> 'ul',
                        'menu_id'=> 'primary-menu',
                        'walker'=> new Custom_Walker_Nav_Menu,
                        )
                  );?>
        </div>
      </div>
    </header>
    <!--end header-->
    <div class="b-regresar">
      <div class="b-regresar__inner"><a href="#" class="b-regresar__link"><span class="icon-flecha-atras"></span>Regresar al listado</a>
      </div>
    </div>
    <div class="b-content">
      <div class="b-content__inner">
        <div class="b-main">
          <div class="b-main__inner">
            <h2 class="b-main__title">Demo of title
            </h2>
            <div class="b-main__main-details">
              <h6 class="b-main__detail"><span class="wrapper-icon"><span class="icon-puesto"></span></span><span class="text">Produccion</span>
              </h6>
              <h6 class="b-main__detail"><span class="wrapper-icon"><span class="icon-ubicacion"></span></span><span class="text">Cercado de lima</span>
              </h6>
              <h6 class="b-main__detail"><span class="wrapper-icon"><span class="icon-calendario"></span></span><span class="text">Publicado hace 3 dias</span>
              </h6>
            </div>
            <div class="b-job-box">
              <h3 class="b-job-box__title">Funciones y responsabilidades
              </h3>
              <div class="b-job-box__content">
                <?php the_content();?>
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
              </div>
            </div>
            <div class="b-job-box">
              <h3 class="b-job-box__title">Requisitos
              </h3>
              <div class="b-job-box__content">
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
                lorem ipsum lorem ipsum lorem ipsum lorem ipsum
              </div>
            </div><?php while( have_posts()): the_post()?>
            <?php the_content();?>
            <?php endwhile;?>
          </div>
        </div>
        <div class="b-aside"><a href="#" class="b-aside__fb b-aside__fb_white"></a><a href="#" class="b-aside__tw b-aside__tw_white"></a><a href="#" class="b-aside__in b-aside__in_white"></a><a href="#" class="b-aside__gp b-aside__gp_white"></a><a href="#" class="b-aside__correo b-aside__correo_white"></a>
        </div>
      </div>
    </div>
    <div class="cf"></div>
    <div class="b-postular">
      <div class="b-postular__inner">
        <button class="b-postular__btn">Postular
        </button>
      </div>
    </div>
    <div class="b-regresar">
      <div class="b-regresar__inner"><a href="#" class="b-regresar__link"><span class="icon-flecha-atras"></span>Regresar al listado</a>
      </div>
    </div>
    <!--start footer-->
    <div class="b-footer">
      <div class="b-footer__inner-footer"><small class="b-footer__credits">&copy; Todos los derechos reservados - 2015</small>
        <div class="b-social-networks"><?php if( get_theme_mod('tcntheme_advanced_options_link_facebook') ):?><a href="<?php echo get_theme_mod('tcntheme_advanced_options_link_facebook');?>" class="b-social-networks__social-network icon icon_facebook"></a><?php endif;?>
          <?php if( get_theme_mod('tcntheme_advanced_options_link_linkedin') ):?><a href="<?php echo get_theme_mod('tcntheme_advanced_options_link_linkedin');?>" class="b-social-networks__social-network icon icon_LinkedIn"></a><?php endif;?>
          <?php if( get_theme_mod('tcntheme_advanced_options_link_twitter') ):?><a href="<?php echo get_theme_mod('tcntheme_advanced_options_link_twitter');?>" class="b-social-networks__social-network icon icon_twitter"></a><?php endif;?>
          <?php if( get_theme_mod('tcntheme_advanced_options_link_googleplus') ):?><a href="<?php echo get_theme_mod('tcntheme_advanced_options_link_googleplus');?>" class="b-social-networks__social-network icon icon_GooglePlus"></a><?php endif;?>
          <?php if( get_theme_mod('tcntheme_advanced_options_link_youtube') ):?><a href="<?php echo get_theme_mod('tcntheme_advanced_options_link_youtube');?>" class="b-social-networks__social-network icon icon_youtube"></a><?php endif;?>
          <?php if( get_theme_mod('tcntheme_advanced_options_link_instagram') ):?><a href="<?php echo get_theme_mod('tcntheme_advanced_options_link_instagram');?>" class="b-social-networks__social-network icon icon_instagram"></a><?php endif;?>
        </div>
      </div>
    </div>
    <!--end footer-->
    <div id="modalLoginUser" class="apt_modal frm_modal fast_login_modal">
      <div class="title align_center"><span>Accede a tu cuenta</span><i class="icon icon_cross"></i></div>
      <form id="frmUserLogIn" action="/mi-cuenta" class="type1">
        <div class="form_box border_grey">
          <div class="form_row">
            <div class="form_col12">
              <fieldset>
                <label>Email</label>
                <input id="txtUser" type="email" name="txtUser" value="" errmsg="No parece ser un correo electrónico valido" placeholder="" size="50" required="">
              </fieldset>
            </div>
          </div>
          <div class="form_row">
            <div class="form_col12">
              <fieldset>
                <label>Contraseña</label>
                <input id="txtPasswordLogin" type="password" name="txtPasswordLogin" value="" errmsg="¡Usa de 3 a 32 caracteres!" size="50" required="">
              </fieldset>
            </div>
          </div>
        </div>
        <button class="btn btn_tertiary mt10"><span class="btn_label">Ingresar</span><span class="btn_spinner"></span></button>
        <p class="mt10"><a href="javascript:;" class="type_white recover_init">Olvidé mi contraseña</a></p>
        <label for="" class="ioption">Recordar mi contraseña
          <input type="hidden" name="chkRemember" value="0">
          <input id="chkRemember" type="checkbox" name="chkRemember" value="1" checked="checked">
          <input id="tipo" type="hidden" name="tipo" value="postulante">
          <input id="hidAuthToken" type="hidden" name="hidAuthToken" value="5813d6ae9fcf95446d9f678acfd7b28f">
        </label>
      </form><a href="http://www.facebook.com/v2.0/dialog/oauth?client_id=287611521299027&amp;redirect_uri=http://aptitus.com/auth/validacion-facebook&amp;scope=email&amp;state=" class="btn_social fb"><span class="logo"></span>Ingresar con Facebook<i class="icon icon_apt_icon_arrow_right"></i></a>
      <p class="row_modal_box">¿No tienes cuenta? <a href="#">Regístrate</a></p>
    </div>
    <div id="modalRegisterUser" class="apt_modal frm_modal fast_register_modal">
      <div class="title align_center"><span>Crear cuenta</span><i class="icon icon_cross"></i></div>
      <form id="frmUserRegistrationFast" method="post" class="type1">
        <div class="message_before_register">Regístrate y descubre cómo puedes obtener el empleo ideal para ti</div>
        <div class="form_box border_grey">
          <div class="form_row">
            <div class="form_col6">
              <fieldset>
                <label>Nombre</label>
                <input id="txtName" type="text" name="txtName" value="" maxlength="75" minlength="2" placeholder="" required="">
              </fieldset>
            </div>
            <div class="form_col6">
              <fieldset>
                <label>Apellido Paterno</label>
                <input id="txtFirstLastName" type="text" name="txtFirstLastName" value="" maxlength="28" minlength="2" errmsg="¡Se requiere su apellido Materno!" placeholder="" required="">
              </fieldset>
            </div>
          </div>
          <div class="form_row">
            <div class="form_col6">
              <fieldset>
                <label>Apellido Materno</label>
                <input id="txtSecondLastName" type="text" name="txtSecondLastName" value="" maxlength="28" minlength="2" errmsg="¡Se requiere su apellido Paterno!" placeholder="" required="">
              </fieldset>
            </div>
            <div class="form_col6">
              <fieldset>
                <label>Fecha de Nacimiento</label>
                <input id="txtBirthDay" type="text" name="txtBirthDay" value="" minyear="1915" maxyear="1997" placeholder="dd/mm/yyyy" required="">
              </fieldset>
            </div>
          </div>
          <div class="form_row">
            <div class="form_col12">
              <fieldset>
                <label>Email</label>
                <input id="txtEmail" type="email" name="txtEmail" value="" maxlength="75" placeholder="" required="">
              </fieldset>
            </div>
          </div>
          <div class="form_row">
            <div class="form_col6">
              <fieldset>
                <label>Contraseña</label>
                <input id="pswd" type="password" name="pswd" value="" maxlength="32" minlength="6" placeholder="" required="">
              </fieldset>
            </div>
            <div class="form_col6">
              <fieldset>
                <label>Confirmar Contraseña</label>
                <input id="pswd2" type="password" name="pswd2" value="" maxlength="32" minlength="3" errmsg="Las contraseñas introducidas no coinciden. Vuelve a intentarlo." placeholder="" required="">
              </fieldset>
            </div>
          </div>
        </div>
        <div class="row mt7">
          <input id="auth_token" type="hidden" name="auth_token" value="dbd5963f91e2bdc5e5b08359868c9507"><span class="conditions">Al crear una cuenta aceptas estar de<br>acuerdo con nuestras <a href="http://centraldeayuda.aptitus.com/forums/20710432-Terminos-y-Condiciones-de-Aptitus" target="_blank" class="type_white">políticas de privacidad, términos y condiciones</a></span>
          <button class="btn btn_tertiary"><span class="btn_label">Crea tu cuenta</span><span class="btn_spinner"></span></button>
        </div>
      </form><a href="http://www.facebook.com/v2.0/dialog/oauth?client_id=287611521299027&amp;redirect_uri=http://aptitus.com/auth/validacion-facebook&amp;scope=email&amp;state=" class="btn_social fb"><span class="logo"></span><span class="name">Conéctate</span> con Facebook<i class="icon icon_apt_icon_arrow_right"></i></a>
      <p class="row_modal_box">¿Ya te encuentras registrado? <a href="#">Ingresa aquí</a></p>
    </div>
    <div id="modalRecoverPassword" class="apt_modal frm_modal recover_password_modal">
      <div class="title align_center"><span>¿Olvidaste tu contraseña?</span></div>
      <form id="frmRecoverPassword" action="/" class="type1">
        <p>Ingresa tu email y te enviaremos una nueva contraseña. Si te logueaste con Facebook ingresa el email de tu cuenta de Facebook</p>
        <fieldset class="input_control_wrapper border_input_control">
          <div>
            <label>Ingresa tu email</label>
            <input id="txtEmailForgot" type="email" name="txtEmailForgot" value="" errmsg="No parece ser un correo electrónico valido" size="50" placeholder="" required="">
          </div>
        </fieldset>
        <button class="btn btn_tertiary mt10"><span class="btn_label">Enviar correo para restablecer contraseña</span><span class="btn_spinner"></span></button>
        <input id="hidRecoverPassword" type="hidden" name="hidRecoverPassword" value="5eb881c26d2df240e5b5aac06d0edb0e">
        <input id="rol" type="hidden" name="rol" value="postulante">
      </form>
    </div>
    <div id="frmPostular" class="b-form frm_modal">
      <div class="b-form__p">
        Para registrar tu postulación necesitamos que contestes
        estar preguntas:
      </div>
      <div class="b-form__fieldset">
        <label>¿Pregunta 1?</label>
        <input id="question1" name="question1">
      </div>
      <div class="b-form__fieldset">
        <label>¿Pregunta 2?</label>
        <input id="question2" name="question2">
      </div>
      <div class="b-form__fieldset">
        <label class="required-fields">Las respuestas son de carácter obligatorio</label>
        <div id="btnEnviarPostula" name="btnEnviarPostula" class="b-form__button">
        </div>
      </div>
    </div>
    <!--scripts--><?php wp_footer();?>
    <script src="<?php echo get_template_directory_uri();?>/js/modules/page-single/index.js?aleatoryxD" type="text/javascript"></script>
  </body>
</html>