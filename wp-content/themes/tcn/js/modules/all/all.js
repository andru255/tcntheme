yOSON.AppCore.addModule("general-fancybox-options", function(){
    return {
        init: function(){
            $.fancybox.defaults.tpl.closeBtn = '<a title="Close" class="fancybox-item fancybox-close" href="javascript:;"><i class="icon icon_close"></i></a>';
        }
    };
}, [
    "js/libs/fancybox/source/jquery.fancybox.pack.js"
]);
yOSON.AppCore.runModule("general-fancybox-options");

yOSON.AppCore.addModule("responsive-user-menu", function(){
    var st = {
        lnkMenu: ".b-responsive-user-menu .icon-sandwich",
        overlay: ".b-responsive-user-menu__overlay",
        menusToShow: ".b-responsive-user-menu__down, .b-responsive-user-menu__down .b-menu"
    };
    var DOM = {};
    var catchDOM = function(){
        DOM.lnkMenu = $(st.lnkMenu);
        DOM.menusToShow = $(st.menusToShow);
        DOM.overlay = $(st.overlay);
    };
    var subscribeEvents = function(){
        DOM.lnkMenu.on("click", events.showMenus);
        DOM.overlay.on("click", events.hideMenus);
    };
    var events = {
        showMenus: function(evt){
           DOM.menusToShow.fadeIn();
           events.showOverlay();
           evt.preventDefault();
        },
        showOverlay: function(){
           DOM.overlay.fadeIn();
        },
        hideMenus: function(){
           DOM.overlay.fadeOut();
           DOM.menusToShow.fadeOut();
        }
    };
    return {
        init: function(){
            $(function(){
                catchDOM();
                subscribeEvents();
            });
        }
    };
});
yOSON.AppCore.runModule("responsive-user-menu");
yOSON.AppCore.addModule("plugins_pretty_select", function(Sb) {
    var dom = {},
        st = {
            tagSelect: ".pretty_select",
            tagSelect2: ".pretty_select_2"
        },
        catchDom = function() {
            dom.tagSelect = $(st.tagSelect);
            dom.tagSelect2 = $(st.tagSelect2);
        },
        afterCatchDom = function() {
            dom.tagSelect2.prettySelect();
        },
        suscribeEvents = function() {

        },
        initialize = function(oP) {
            $.extend(st, oP);
            catchDom();
            afterCatchDom();
            suscribeEvents();
        };
    return {
        init: initialize
    };
}, ["js/libs/pretty-select.js"]);
yOSON.AppCore.runModule("plugins_pretty_select");

yOSON.AppCore.addModule("modal_switcher", function(Sb) {
    var factory = function(oP) {
        this.st = {
            contentAll: ".page_default",
            pageModal: ".page_modal",
            modal: null,
            title: ".title",
            btnCloseModal: ".icon_cross",
            btnShowModal: null,
            fancyBoxSetting: null,
            allErrors: ".error",
            device_mobile: device.mobile()
        };
        this.dom = {};
        this.oP = oP;
    };
    factory.prototype = {
        catchDom: function() {
            this.dom.contentAll = $(this.st.contentAll);
            this.dom.modal = $(this.st.modal);
            this.dom.pageModal = $(this.st.pageModal);
            this.dom.title = $(this.st.title, this.dom.modal);
            this.dom.btnCloseMobileModal = $(this.st.btnCloseModal, this.dom.title);
            this.dom.btnShowModal = $(this.st.btnShowModal);
            this.dom.window = $(window);
        },
        afterCatchDom: function() {
            this.st.device_mobile && (this.dom.title.removeClass("title"), this.dom.title.addClass("sub_title_mobile"));
        },
        suscribeEvents: function() {
            this.dom.btnShowModal.on("click", {
              inst: this
            }, this.eShowModal);

            this.st.device_mobile && this.dom.btnCloseMobileModal.on("click", {
                inst: this
            }, this.eHideModalMobile);
        },
        eHideModalMobile: function(event) {
            var dom, st, that;
            event.stopPropagation();
            that = event.data.inst;
            st = that.st;
            dom = that.dom;
            that.fnHideModal();
        },
        eShowModal: function(event) {
            var _this, dom, st, that;
            event.preventDefault();
            that = event.data.inst;
            st = that.st;
            dom = that.dom;
            _this = $(this);
            if("function" == typeof st.beforeShowModalCallback) {
                st.beforeShowModalCallback(_this);
            }
            if(st.device_mobile) {
                dom.contentAll.addClass("hide");
                dom.pageModal.addClass("hide");
                dom.modal.removeClass("hide");
                dom.window.scrollTop(0);
                if("function" == typeof st.showModalCallBack){
                    st.showModalCallBack(_this);
                }
            }else{
                if("function" == typeof st.showModalMobileCallBack){
                    st.fancyBoxSetting.afterLoad = st.showModalMobileCallBack;
                } 
                st.fancyBoxSetting.afterClose = function() {
                    $(st.allErrors, dom.modal).removeClass("error");
                    if( "function" == typeof st.hideModalMobileCallBack ){
                        st.hideModalMobileCallBack(_this);
                    }
                };
                $.fancybox.close();
                $.fancybox.open(st.modal, st.fancyBoxSetting);
                $.prettySelect.update();
            }
        },
        fnHideModal: function() {
            var dom, st;
            dom = this.dom;
            st = this.st;
            dom.contentAll.removeClass("hide");
            dom.modal.addClass("hide");
            $(st.allErrors, dom.modal).removeClass("error");
            st.hideModalCallBack();
        },
        execute: function() {
            this.st = $.extend({}, this.st, this.oP);
            this.st.beforeShowModalCallback = this.oP.beforeShowModal;
            this.st.showModalCallBack = this.oP.showModal;
            this.st.showModalMobileCallBack = this.oP.showModalMobile;
            this.st.hideModalCallBack = this.oP.hideModal;
            this.st.hideModalMobileCallBack = this.oP.hideModalMobile;
            this.catchDom();
            this.afterCatchDom();
            this.suscribeEvents();
        }
    };

    var publicInitialize = function(arrOp) {
        $.each(arrOp, function(i, obj) {
            var instance;
            instance = new factory(obj);
            instance.execute();
        });
    },
    fnCloseModal = function(arr) {
        var dom;
        dom = {
            contentAll: $(".page_default"),
            pageModal: $(".page_modal:visible")
        };
        if(device.mobile()){
            dom.contentAll.removeClass("hide");
            dom.pageModal.addClass("hide");
            $(".error", dom.modal).removeClass("error");
        } else {
            $.fancybox.close();
        }
    },
    fnOpenModal = function(idModal, paramsFancybox) {
        if(device.mobile()){
            $(".page_default").addClass("hide"), $(idModal).removeClass("hide");
        } else {
            $.fancybox.open(idModal, paramsFancybox);
        }
    },
    initialize = function() {
        Sb.events(["fnCloseModal"], fnCloseModal, this);
        Sb.events(["fnOpenModal"], fnOpenModal, this);
        Sb.events(["modalSwitcherReInitialize"], publicInitialize, this);
    };
    return {
        init: initialize
    };
}, [
    "js/libs/fancybox/source/jquery.fancybox.pack.js",
    "js/libs/pretty-select.js"
]);
yOSON.AppCore.runModule("modal_switcher");

yOSON.AppCore.addModule("validate_forms", function(Sb) {
    var dom = {},
        st = {
          frmType1: "form.type1,.form_valide_tooltip",
          fieldset: "fieldset",
          inputs: "input,textarea",
          classSelected: "selected",
          classError: "error"
        },
        beforeCatchDom = function() {
          functions.configMessages();
          functions.validateExtraFunctions();
        },
        catchDom = function() {
          dom.window = $(window);
          dom.frmType1 = $(st.frmType1);
          dom.fieldset = $(st.fieldset, dom.frmType1);
          dom.inputs = $(st.inputs, dom.frmType1);
        },
        afterCatchDom = function() {
          if ( dom.frmType1.length ) {
            functions.initTooltip();
            functions.initFormValidate();
          }
        },
        suscribeEvents = function() {
          $(document).on("click", st.fieldset, events.showInputSelected);
          $(document).on("blur", st.inputs, events.removeClassSelected);
          $(document).on("focus", st.inputs, events.addClassSelected);
        },
        events = {
          showInputSelected: function(e) {
            var _this, _thisInput;
            _this = $(this);
            _thisInput = $(st.inputs, _this);
            if(!$("select", _this).length){
                if(_thisInput.is(":checkbox") && _thisInput.is(":radio") && !_thisInput.is("textarea")) { 
                    _this.parents(st.frmType1).find("." + st.classSelected).removeClass(st.classSelected);
                    _this.addClass(st.classSelected);
                    _thisInput.focus();
                }
            }
          },
          removeClassSelected: function() {
              var _thisInput;
              _thisInput = $(this);
              _thisInput.parents(st.fieldset).removeClass(st.classSelected);
          },
          addClassSelected: function() {
              var _thisInput;
              _thisInput = $(this);
              _thisInput.parents(st.fieldset).addClass(st.classSelected);
          }
        },
        functions = {
          initTooltip: function() {
              $.fn.tooltipster("setDefaults", {
                  trigger: "custom",
                  multiple: true,
                  updateAnimation: false,
                  timer: 1000
              });
              $(st.fieldset).tooltipster();
          },
          initFormValidate: function() {
            $.validator.setDefaults({
                onfocusout: false,
                errorPlacement: function(error, element) {
                    var _parent, lastError, newError;
                    _parent = $(element).parents(st.fieldset);
                    newError = $(error).text();
                    lastError = _parent.data("lastError");
                    _parent.data("lastError", newError);
                    _parent.removeClass(st.classSelected);
                    _parent.addClass(st.classError);
                    if( newError != "" && newError !== lastError){
                        _parent.tooltipster("content", newError);
                        functions.changePositionTooltip(_parent);
                    }
                    _parent.tooltipster("show");
                },
                success: function(label, element) {
                    var _parent;
                    _parent = $(element).parents(st.fieldset);
                    _parent.tooltipster("hide");
                    _parent.removeClass(st.classError);
                    _parent.removeClass(st.classSelected);
                }
            });
           },
           changePositionTooltip: function(_this) {
               var posHtml, posWrap;
               posHtml = dom.window.scrollTop();
               posWrap = parseInt(_this.parents("form").offset().top) - 150;
               if( posHtml > posWrap ) {
                   _this.tooltipster("option", "position", "bottom");
               } else {
                   _this.tooltipster("option", "position", "top");
               }
           },
           configMessages: function() {
               $.extend($.validator.messages, {
                   required: "Este campo es requerido",
                   equalTo: "El valor debe ser idéntico",
                   email: "Ingrese un email válido"
               });
           },
           validateExtraFunctions: function() {
            $.validator.addMethod("alphNumeric", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑäëïöüÿÄËÏÖÜ ]+$/gi.test(value);
            }, "Solo letras y números.");
            $.validator.addMethod("alphabet", function(value, element) {
                return this.optional(element) ||  /^[a-zA-ZáéíóúÁÉÍÓÚñÑ&äëïöüÿÄËÏÖÜ ]+$/gi.test(value);
            }, "Solo letras.");
            $.validator.addMethod("comment", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9áéíóúÁÉÍÓÚñÑ&\.,äëïöüÿÄËÏÖÜ\?¿!¡\-_\*;:\+\(\)#%\$@=\"\'\/\n ]+$/gi.test(value);
            }, "Texto inválido.");
            $.validator.addMethod("dateMask", function(value, element) {
                return this.optional(element) || /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/gi.test(value);
            }, "Fecha inválida.");
            $.validator.addMethod("nEmail", function(value, element) {
                return this.optional(element) || /^[a-z0-9\_\-]+(\.?[a-z0-9\_\-]+)+@\w+([\.-]?\w+)+$/i.test(value);
            }, "El email ingresado es incorrecto.");
            $.validator.addMethod("lessThanEqual", function(value, element, param) {
                return this.optional(element) || parseInt(value) >= parseInt($(param).val());
            }, "El año fin debe ser mayor al año de inicio");
            $.validator.addMethod("lessThanEqualMonth", function(value, element, param) {
                var flag, valueMonthBegin, valueMonthEnd, valueYearBegin, valueYearEnd;
                return valueMonthBegin = parseInt($(param[0]).val()), valueYearBegin = parseInt($(param[1]).val()), valueYearEnd = parseInt($(param[2]).val()), valueMonthEnd = parseInt(value), flag = !0, valueYearBegin === valueYearEnd && valueMonthBegin > valueMonthEnd && (flag = !1), this.optional(element) || flag;
            }, "El mes debe ser mayor o igual al mes de inicio");
            $.validator.addMethod("currentMonth", function(value, element, param) {
                var currentMonth, currentYear, flag, valueMonthEnd, valueYearBegin, valueYearEnd;
                return valueYearBegin = parseInt($(param[0]).val()), valueYearEnd = parseInt($(param[1]).val()), currentMonth = parseInt($(param[1]).attr("data-currentmonth")), currentYear = parseInt($(param[1]).attr("data-current-year")), valueMonthEnd = parseInt(value), flag = !0, valueYearBegin === currentYear && valueYearEnd === currentYear && valueMonthEnd > currentMonth && (flag = !1), this.optional(element) || flag;
            }, "El mes ingresado es mayor a la mes actual");
            $.validator.addMethod("autocompleteValidator", function(value, element, param) {
                var flag, idElement;
                return idElement = $(param).val(), flag = !0, "" === $.trim(idElement) && (flag = !1), this.optional(element) || flag;
            }, "Seleccione una opción de las sugerencias que le brindamos.");
        }
    }, initialize = function(oP) {
        $.extend(st, oP);
        beforeCatchDom();
        catchDom();
        afterCatchDom();
        suscribeEvents();
    };
    return {
        init: initialize
    };
}, ["js/libs/jquery.validate.min.js", "js/libs/messages_es.js", "js/libs/jquery.tooltipster.min.js"]);
yOSON.AppCore.runModule("validate_forms");

yOSON.AppCore.addModule("modal_login_form", function(Sb) {
    var dom = {},
        st = {
           modalLoginUser: "#modalLoginUser",
           modalRegisterUser: "#modalRegisterUser",
           btnLoginInit: ".b-nav-login__login",
           returnNormal: null,
           frmUserLogIn: "#frmUserLogIn",
           txtUser: "#txtUser",
           txtPasswordLogin: "#txtPasswordLogin",
           hidAuthToken: "#hidAuthToken",
           chkTipo: "#chkTipo",
           btnForm: "button",
           lnkChangeModal: ".row_modal_box a",
           btn_social: ".btn_social",
           urlAjax: "http://aptitus.com/auth/new-login-ajax/",
           dropRightMenu: "#dropRightMenu",
           showRightPush: "#showRightPush"
        },
        catchDom = function() {
            dom.modalLoginUser = $(st.modalLoginUser);
            dom.frmUserLogIn = $(st.frmUserLogIn);
            dom.txtUser = $(st.txtUse, dom.frmUserLogIn);
            dom.txtPasswordLogin = $(st.txtPasswordLogin, dom.frmUserLogIn);
            dom.hidAuthToken = $(st.hidAuthToken, dom.frmUserLogIn);
            dom.btnForm = $(st.btnForm, dom.frmUserLogIn);
            dom.chkTipo = $(st.chkTipo, dom.frmUserLogIn);
            dom.lnkChangeModal = $(st.lnkChangeModal, dom.modalLoginUser);
            dom.btn_social = $(st.btn_social, dom.modalLoginUser);
            dom.dropRightMenu = $(st.dropRightMenu);
            dom.showRightPush = $(st.showRightPush);
        },
        afterCatchDom = function() {
            st.returnNormal = dom.frmUserLogIn.attr("action");
            fn.initValidateForm();
            Sb.trigger("modalSwitcherReInitialize", [{
                modal: st.modalLoginUser,
                btnShowModal: st.btnLoginInit,
                fancyBoxSetting: {
                    maxWidth: 300,
                    padding: 0,
                    arrows: !1
                },
                beforeShowModal: function(_this) {
                    fn.setClassNecessary(); fn.setRedirectAjax(_this);
                    fn.setRolLogin(_this);
                },
                hideModal: function() {
                    dom.txtUser.val(""), dom.txtPasswordLogin.val("");
                },
                hideModalMobile: function() {
                    dom.txtUser.val(""), dom.txtPasswordLogin.val("");
                }
            }]);
        },
        fn = {
            changeModal: function(e) {
                e.preventDefault();
                Sb.trigger("fnCloseModal");
                Sb.trigger("fnOpenModal", st.modalRegisterUser, {
                  maxWidth: 464,
                  padding: 0,
                  arrows: !1
                });
             },
             initValidateForm: function() {
                dom.frmUserLogIn.tooltipster({
                   timer: 1500
                });
                dom.frmUserLogIn.validate({
                   rules: {
                      txtUser: {
                          nEmail: !0
                      },
                      txtBirthDay: {
                          date: !0
                      }
                   },
                   submitHandler: function(form) {
                      dom.btnForm.attr({
                        disabled: "",
                        loading: ""
                      });
                      $.ajax({
                        url: st.urlAjax,
                        type: "POST",
                        dataType: "json",
                        data: dom.frmUserLogIn.serialize(),
                        success: function(response) {
                            if(response.status === "1"){
                                window.location = st.returnNormal;
                            } else {
                                dom.hidAuthToken.val(response.hashToken);
                                dom.frmUserLogIn.tooltipster("content", response.msg);
                                dom.frmUserLogIn.tooltipster("show");
                                dom.btnForm.removeAttr("disabled loading");
                                dom.txtPasswordLogin.val("");
                                dom.txtUser.parent().addClass("error"), dom.txtPasswordLogin.focus();
                            }
                        },
                        error: function(response) {
                            dom.btnForm.removeAttr("disabled loading");
                        }
                      });
                   }
                });
             },
             setClassNecessary: function() {
                 dom.showRightPush.removeClass("active");
                 $("body").removeClass("active_menu_push_toleft");
                 dom.dropRightMenu.removeClass("active_menu_open");
             },
             setRedirectAjax: function(_this) {
                 void 0 !== _this.data("redirect") && (st.returnNormal = _this.data("redirect"));
             },
             setRolLogin: function(_this) {
                 var rol;
                 rol = _this.data("type");
                 void 0 === rol && (rol = "postulante");
                 dom.btn_social.show();
                 dom.lnkChangeModal.attr("href", "#").on("click.changeModal", fn.changeModal);
                 dom.chkTipo.val(rol);
             }
        },
        initialize = function(oP) {
            $.extend(st, oP), catchDom(), afterCatchDom();
        };
        return {
           init: initialize
        };
}, [
    "js/libs/jquery.validate.min.js",
    "js/libs/jquery.tooltipster.min.js",
    "js/libs/jquery.device.js"
]);
yOSON.AppCore.runModule("modal_login_form");

yOSON.AppCore.addModule('modal_register_form', function (Sb) {
   var st = {
       modalLoginUser: '#modalLoginUser',
       modalRegisterUser: '#modalRegisterUser',
       btnRegisterInit: '.b-nav-login__signup',
       frmUserRegistrationFast: '#frmUserRegistrationFast',
       txtBirthDay: '#txtBirthDay',
       hidAuthToken: '#auth_token',
       btnForm: 'button',
       lnkChangeModal: '.row_modal_box',
       urlAjax: '/registro/registro-rapido',
       dropRightMenu: '#dropRightMenu',
       showRightPush: '#showRightPush',
       body: 'body'
   },
   dom = {},
   catchDom = function () {
       dom.modalRegisterUser = $(st.modalRegisterUser);
       dom.frmUserRegistrationFast = $(st.frmUserRegistrationFast);
       dom.txtBirthDay = $(st.txtBirthDay, dom.frmUserRegistrationFast);
       dom.hidAuthToken = $(st.hidAuthToken, dom.frmUserRegistrationFast);
       dom.btnForm = $(st.btnForm, dom.frmUserRegistrationFast);
       dom.lnkChangeModal = $(st.lnkChangeModal, dom.modalRegisterUser);
       dom.dropRightMenu = $(st.dropRightMenu);
       dom.showRightPush = $(st.showRightPush);
       dom.body = $(st.body);
   },
   afterCatchDom = function () {
       dom.txtBirthDay.inputmask('date', {
           yearrange: {
               minyear: dom.txtBirthDay.attr('minyear'),
               maxyear: dom.txtBirthDay.attr('maxyear')
           }
       });
       functions.initValidateForm();
       Sb.trigger('modalSwitcherReInitialize', [
           {
               modal: st.modalRegisterUser,
               btnShowModal: st.btnRegisterInit,
               fancyBoxSetting: {
                   maxWidth: 464,
                   padding: 0,
                   arrows: !1
               },
               beforeShowModal: function (_this) {
                   _this = $(this);
                   dom.showRightPush.removeClass('active');
                   dom.body.removeClass('active_menu_push_toleft');
                   dom.dropRightMenu.removeClass('active_menu_open');
               },
               hideModal: function () {
                   $(':input,:password', dom.modalRegisterUser).val('');
               },
               hideModalMobile: function () {
                   $(':input,:password', dom.modalRegisterUser).val('');
               }
           }
       ]);
   },
   suscribeEvents = function () {
       dom.lnkChangeModal.on('click', 'a', functions.changeModal);
   },
   functions = {
       changeModal: function (e) {
           e.preventDefault();
           Sb.trigger('fnCloseModal');
           Sb.trigger('fnOpenModal', st.modalLoginUser, {
               maxWidth: 300,
               padding: 0,
               arrows: !1
           });
       },
       initValidateForm: function () {
           dom.frmUserRegistrationFast.tooltipster({
               timer: 1500
           });
           dom.frmUserRegistrationFast.validate({
               rules: {
                   pswd2: {
                       equalTo: '#pswd'
                   },
                   txtName: {
                       alphabet: !0
                   },
                   txtFirstLastName: {
                       alphabet: !0
                   },
                   txtSecondLastName: {
                       alphabet: !0
                   },
                   txtBirthDay: {
                       dateMask: !0
                   },
                   txtEmail: {
                       nEmail: !0
                   }
               },
               submitHandler: function (form) {
                   dom.btnForm.attr({
                       disabled: '',
                       loading: ''
                   });
                   $.ajax({
                       url: st.urlAjax,
                       type: 'POST',
                       dataType: 'json',
                       data: dom.frmUserRegistrationFast.serialize(),
                       success: function (response) {
                           if(response.status){
                               yOSON.utils.showMessage($('.message_before_register', dom.frmUserRegistrationFast), 'success', response.message);
                               setTimeout(function () {
                                   window.location = yOSON.baseHost + response.redirect;
                               }, 1000);
                           } else {
                               yOSON.utils.showMessage($('.message_before_register', dom.frmUserRegistrationFast), 'error', response.message);
                               dom.hidAuthToken.val(response.hashToken);
                               dom.btnForm.removeAttr('disabled loading');
                           }
                       },
                       error: function (response) {
                           dom.btnForm.removeAttr('disabled loading');
                       }
                   });
               }
          });
       }
   },
   initialize = function (oP) {
       $.extend(st, oP);
       catchDom();
       afterCatchDom();
       suscribeEvents();
   };
   return {
       init: initialize
   };
} , ['js/libs/jquery.inputmask.bundle.min.js']);
yOSON.AppCore.runModule("modal_register_form");

yOSON.AppCore.addModule("modal_recover_password", function(Sb) {
    var dom = {},
        st = {
          modalRecoverPassword: "#modalRecoverPassword",
          btnRecover: ".recover_init",
          frmRecoverPassword: "#frmRecoverPassword",
          txtEmailForgot: "#txtEmailForgot",
          hidRecoverPassword: "#hidRecoverPassword",
          btnForm: "button",
          urlAjax: "/auth/new-recuperar-clave/"
        },
        catchDom = function() {
          dom.frmRecoverPassword = $(st.frmRecoverPassword);
          dom.txtEmailForgot = $(st.txtEmailForgot);
          dom.hidRecoverPassword = $(st.hidRecoverPassword);
          dom.btnForm = $(st.btnForm, dom.frmRecoverPassword);
        },
        afterCatchDom = function() {
          st.returnNormal = dom.frmRecoverPassword.attr("action");
          functions.initValidateForm();
        },
        suscribeEvents = function() {
          Sb.trigger("modalSwitcherReInitialize", [{
            modal: st.modalRecoverPassword,
            btnShowModal: st.btnRecover,
            fancyBoxSetting: {
              maxWidth: 500,
              padding: 0,
              arrows: !1
            },
            hideModal: function() {
              dom.txtEmailForgot.val("");
            },
            hideModalMobile: function() {
              dom.txtEmailForgot.val("");
            }
          }]);
        },
        functions = {
          initValidateForm: function() {
            dom.frmRecoverPassword.tooltipster({
              timer: 1500
            });
            dom.frmRecoverPassword.validate({
              rules: {
                txtEmailForgot: {
                  nEmail: !0
                }
              },
              submitHandler: function(form) {
                dom.btnForm.attr({
                  disabled: "",
                  loading: ""
                });
                $.ajax({
                   url: st.urlAjax,
                   type: "POST",
                   dataType: "json",
                   data: dom.frmRecoverPassword.serialize(),
                   success: function(response) {
                     if( "1" === response.status ){
                       window.location = st.returnNormal;
                     } else {
                        dom.hidRecoverPassword.val(response.hashToken);
                        dom.frmRecoverPassword.tooltipster("content", response.msg);
                        dom.frmRecoverPassword.tooltipster("show");
                        dom.btnForm.removeAttr("disabled loading");
                        dom.txtEmailForgot.val("");
                        dom.txtEmailForgot.parent().addClass("error");
                        dom.txtEmailForgot.focus(); 
                     }
                   },
                   error: function(response) {
                     dom.btnForm.removeAttr("disabled loading");
                   }
                });
             }
            });
          }
        },
        initialize = function(oP) {
            $.extend(st, oP);
            catchDom();
            afterCatchDom();
            suscribeEvents();
        };
        return {
          init: initialize
        };
}, []);
yOSON.AppCore.runModule("modal_recover_password");

