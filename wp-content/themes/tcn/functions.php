<?php
//append the walker menu
include( 'custom-walker-menu.php' );
//append the custom favicon control
include( 'customize-favicon-control.php' );
function tcntheme_wp_title( $title, $sep ) {
    global $paged, $page;
    
    if ( is_feed() )
        return $title;
    
    // Add the site name.
    $title .= get_bloginfo( 'name', 'display' );
    
    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        $title = "$title $sep $site_description";
    
    // Add a page number if necessary.
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() )
        $title = "$title $sep " . sprintf( __( 'Page %s', 'twentythirteen' ), max( $paged, $page ) );
    
    return $title;
};
add_filter( 'wp_title', 'tcntheme_wp_title', 10, 2 );

function tcntheme_vars_css(){
    $output = '<style type="text/css">';

    if(get_theme_mod('tcntheme_advanced_options_header_color')){
        $output .= '.b-main-menu{';
        $output .= 'background-color:'. get_theme_mod('tcntheme_advanced_options_header_color').' !important;';
        $output .= '}';
    }

    $output .= '.b-menu__link{';
    if(get_theme_mod('tcntheme_advanced_options_menu_background_color')){
        $output .= 'background-color:'. get_theme_mod('tcntheme_advanced_options_menu_background_color').' !important;';
    }
    if(get_theme_mod('tcntheme_advanced_options_menu_font_color')){
        $output .= 'color:'. get_theme_mod('tcntheme_advanced_options_menu_font_color').' !important;';
    }
    $output .= '}';

    $output .= '.b-footer, .b-footer__inner-footer{';
    if(get_theme_mod('tcntheme_advanced_options_footer_color')){
        $output .= 'background-color:'. get_theme_mod('tcntheme_advanced_options_footer_color').' !important;';
    }
    $output .= '}';

    $output .= '.b-footer__credits{';
    if(get_theme_mod('tcntheme_advanced_options_credits_color')){
        $output .= 'color:'. get_theme_mod('tcntheme_advanced_options_credits_color').' !important;';
    }
    $output .= '}';

    $output .= '.b-social-networks__social-network{';
    if(get_theme_mod('tcntheme_advanced_options_icon_color')){
        $output .= 'color:'. get_theme_mod('tcntheme_advanced_options_icon_color').' !important;';
    }
    $output .= '}';

    $output .= '</style>';
    echo $output;
};
add_action( 'wp_head', 'tcntheme_vars_css');

function tcntheme_vars_js(){
    $output = '<script>';
    $output .= 'var yOSON = {';
    $output .= '"statHost":"'.get_template_directory_uri().'/",';
    $output .= '"statVers":"?'.date("YmdG").'"';
    $output .= '};';
    $output .= '</script>';
    echo $output;
};
add_action( 'wp_head', 'tcntheme_vars_js');

function tcntheme_setup(){
    register_nav_menu( 'primary', __('Navigation Menu', 'tcntheme' ) );
};
add_action('after_setup_theme', 'tcntheme_setup');

function tcntheme_options( $wp_customize ){
    //setting the blogname
    $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
    $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
    //setting advanced panel
    $wp_customize->add_section(
        'tcntheme_advanced_options',
        array(
            'title' => __('Personalizar', 'tcntheme'),
            'priority' => 100,
            'capability' => 'edit_theme_options',
            'description' => __('Change advanced settings here', 'tcntheme')
        )
    );
    //setting advanced panel - social network links
    $wp_customize->add_section(
        'tcntheme_social_network_options',
        array(
            'title' => __('Redes sociales', 'tcntheme'),
            'priority' => 100,
            'capability' => 'edit_theme_options',
            'description' => __('Ingresa las urls de las redes sociales', 'tcntheme')
        )
    );
    //setting advanced panel - additional info
    $wp_customize->add_section(
        'tcntheme_add_info',
        array(
            'title' => __('Informacion Adicional', 'tcntheme'),
            'priority' => 100,
            'capability' => 'edit_theme_options',
            'description' => __('Ingresa información adicional', 'tcntheme')
        )
    );
    //setting favicon
    $wp_customize->add_setting(
        'tcntheme_advanced_options_favicon',
        array(
            'transport' => 'postMessage',
            'default' => ''
        )
    );
    $wp_customize->add_control( new Custom_Favicon_Control(
        $wp_customize,
        'tcntheme_advanced_controls_favicon',
        array(
            'label' => __('favicon', 'tcntheme'),
            'section' => 'tcntheme_advanced_options',
            'settings' => 'tcntheme_advanced_options_favicon',
            'priority' => 10
        )
    ));
    //setting logo
    $wp_customize->add_setting(
        'tcntheme_advanced_options_logo',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control( new WP_Customize_Upload_Control(
        $wp_customize,
        'tcntheme_advanced_controls_logo',
        array(
            'label' => __('Logo', 'tcntheme'),
            'section' => 'tcntheme_advanced_options',
            'settings' => 'tcntheme_advanced_options_logo',
            'priority' => 10
        )
    ));
    //setting banner image
    $wp_customize->add_setting(
        'tcntheme_advanced_options_banner_image',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control( new WP_Customize_Upload_Control(
        $wp_customize,
        'tcntheme_advanced_controls_banner_image',
        array(
            'label' => __('Imagen de Banner', 'tcntheme'),
            'section' => 'tcntheme_advanced_options',
            'settings' => 'tcntheme_advanced_options_banner_image',
            'priority' => 10
        )
    ));
    //setting background header color
    $wp_customize->add_setting(
        'tcntheme_advanced_options_header_color',
        array(
            'transport' => 'postMessage',
            'default' => ''
        )
    );
    $wp_customize->add_control( new WP_Customize_Color_Control(
        $wp_customize,
        'tcntheme_advanced_controls_header_color',
        array(
            'label' => __('Header Background Color', 'tcntheme'),
            'section' => 'tcntheme_advanced_options',
            'settings' => 'tcntheme_advanced_options_header_color',
            'priority' => 10
        )
    ));
    //setting menu background color
    $wp_customize->add_setting(
        'tcntheme_advanced_options_menu_background_color',
        array(
            'transport' => 'postMessage',
            'default' => ''
        )
    );
    $wp_customize->add_control( new WP_Customize_Color_Control(
        $wp_customize,
        'tcntheme_advanced_controls_menu_background_color',
        array(
            'label' => __('Menu Background Color', 'tcntheme'),
            'section' => 'tcntheme_advanced_options',
            'settings' => 'tcntheme_advanced_options_menu_background_color',
            'priority' => 10
        )
    ));
    //setting menu font color
    $wp_customize->add_setting(
        'tcntheme_advanced_options_menu_font_color',
        array(
            'transport' => 'postMessage',
            'default' => ''
        )
    );
    $wp_customize->add_control( new WP_Customize_Color_Control(
        $wp_customize,
        'tcntheme_advanced_controls_menu_font_color',
        array(
            'label' => __('Menu Fonts Color', 'tcntheme'),
            'section' => 'tcntheme_advanced_options',
            'settings' => 'tcntheme_advanced_options_menu_font_color',
            'priority' => 10
        )
    ));
    //setting background footer color
    $wp_customize->add_setting(
        'tcntheme_advanced_options_footer_color',
        array(
            'transport' => 'postMessage',
            'default' => ''
        )
    );
    $wp_customize->add_control( new WP_Customize_Color_Control(
        $wp_customize,
        'tcntheme_advanced_controls_footer_color',
        array(
            'label' => __('Footer Background Color', 'tcntheme'),
            'section' => 'tcntheme_advanced_options',
            'settings' => 'tcntheme_advanced_options_footer_color',
            'priority' => 10
        )
    ));
    //setting credits color
    $wp_customize->add_setting(
        'tcntheme_advanced_options_credits_color',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control( new WP_Customize_Color_Control(
        $wp_customize,
        'tcntheme_advanced_controls_credits_color',
        array(
            'label' => __('Icons Color', 'tcntheme'),
            'section' => 'tcntheme_advanced_options',
            'settings' => 'tcntheme_advanced_options_credits_color',
            'priority' => 10
        )
    ));
    //setting socialnetwork icons color
    $wp_customize->add_setting(
        'tcntheme_advanced_options_icon_color',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control( new WP_Customize_Color_Control(
        $wp_customize,
        'tcntheme_advanced_controls_icon_color',
        array(
            'label' => __('Icons Color', 'tcntheme'),
            'section' => 'tcntheme_advanced_options',
            'settings' => 'tcntheme_advanced_options_icon_color',
            'priority' => 10
        )
    ));
    //==========================================================//
    $redes_sociales = array("facebook", "linkedin", "twitter", "googleplus", "youtube", "instagram");
    for($i = 0; $i < count($redes_sociales); $i++){
        $wp_customize->add_setting(
            'tcntheme_advanced_options_link_'. $redes_sociales[$i],
            array(
                'default' => 'http://'.$redes_sociales[$i].'.com'
            )
        );
        $wp_customize->add_control( new WP_Customize_Control(
            $wp_customize,
            'tcntheme_advanced_controls_link_'. $redes_sociales[$i],
            array(
                'label' => __($redes_sociales[$i], 'tcntheme'),
                'section' => 'tcntheme_social_network_options',
                'settings' => 'tcntheme_advanced_options_link_'. $redes_sociales[$i],
                'priority' => 10
            )
        ));
    }
    //==========================================================//
    //setting RUC
    $wp_customize->add_setting(
        'tcntheme_add_info_options_ruc',
        array(
            'default' => ''
        )
    );
    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'tcntheme_add_info_controls_ruc',
        array(
            'label' => __('RUC', 'tcntheme'),
            'section' => 'tcntheme_add_info',
            'settings' => 'tcntheme_add_info_options_ruc',
            'priority' => 10
        )
    ));

};
add_action('customize_register', 'tcntheme_options');

function tcntheme_customizer_live_preview(){
    wp_enqueue_script(
        'tcntheme_customizer',
        get_template_directory_uri(). '/js/theme-customizer.js',
        array(
            'customize-preview'
        ),
        '20150830',
        true
    );
}
add_action('customize_preview_init', 'tcntheme_customizer_live_preview');

//creating the custom field for the teaser content
function tcntheme_teaser_content(){
    add_meta_box(
        "teaser_content",
        __( "Contenido en el teaser", 'tcntheme'),
        'tcntheme_teaser_content_callback',
        'page'
    );
}
//callback for render my custom field
function tcntheme_teaser_content_callback($post){
    // Add a nonce field so we can check for it later.
    wp_nonce_field( 'tcntheme_teaser_title_save_meta_box_data', 'tcntheme_teaser_title_meta_box_nonce' );
    $value_title = get_post_meta($post->ID, 'teaser_title_field', true);
    echo '<p>';
    echo '<label for="teaser_title_field">';
    _e('Ingresa el título que se mostrará en el teaser de la página', 'tcntheme');
    echo '</label>';
    echo '</p>';
    echo '<input id="teaser_title_field" name="teaser_title_field" class="regular-text" value="'.esc_attr( $value_title ).'" />';

    // Add a nonce field so we can check for it later.
    wp_nonce_field( 'tcntheme_save_meta_box_data', 'tcntheme_meta_box_nonce' );
    $value_content = get_post_meta($post->ID, 'teaser_content_field', true);
    echo '<p>';
    echo '<label for="teaser_content_field">';
    _e('Ingresa el contenido que se mostrará en el teaser de la página', 'tcntheme');
    echo '</label>';
    echo '</p>';
    echo '<div class="wp-editor-container">';
    echo '<textarea id="teaser_content_field" name="teaser_content_field" class="wp-editor-area">'.esc_attr( $value_content ).'</textarea>';
    echo '</div>';
}
add_action("add_meta_boxes", "tcntheme_teaser_content");

function tcntheme_teaser_content_save_data($post_id){
    /*
     * We need to verify this came from our screen and with proper authorization,
     * because the save_post action can be triggered at other times.
     */

    // Check if our nonce is set.
    if ( ! isset( $_POST['teaser_title_field'] ) ) {
        return;
    }

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['tcntheme_teaser_title_meta_box_nonce'], 'tcntheme_teaser_title_save_meta_box_data' ) ) {
        return;
    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Check the user's permissions.
    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return;
        }

    } else {

        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
    }

    // Check if our nonce is set.
    if ( ! isset( $_POST['teaser_content_field'] ) ) {
        return;
    }

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['tcntheme_meta_box_nonce'], 'tcntheme_save_meta_box_data' ) ) {
        return;
    }

    /* OK, it's safe for us to save the data now. */
    
    // Sanitize user input.
    $my_data = $_POST['teaser_content_field'];

    // Update the meta field in the database.
    update_post_meta( $post_id, 'teaser_content_field', $my_data );

    // Sanitize user input.
    $my_data = $_POST['teaser_title_field'];

    // Update the meta field in the database.
    update_post_meta( $post_id, 'teaser_title_field', $my_data );
}
add_action("save_post", "tcntheme_teaser_content_save_data");

?>
